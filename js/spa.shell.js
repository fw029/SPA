spa.shell = (function (){
    var
        configMap = {
            // 定义给uriAnchor使用的映射，用于验证
            anchor_schema_map: {
                chat:{open: true, closed: true}
            },
            main_html: String()
                + '<div class="spa-shell-head">'
                    + '<div class="spa-shell-head-logo"></div>'
                    + '<div class="spa-shell-head-acct"></div>'
                    + '<div class="spa-shell-head-search"></div>'
                + '</div>'
                + '<div class="spa-shell-main">'
                    + '<div class="spa-shell-main-nav"></div>'
                    + '<div class="spa-shell-main-content"></div>'
                + '</div>'
                + '<div class="spa-shell-foot"></div>'
                + '<div class="spa-shell-chat"></div>'
                + '<div class="spa-shell-modal"></div>',
            chat_extend_time: 250,
            chat_retract_time: 300,
            chat_extend_height: 450,
            chat_retract_height: 15,
            chat_extended_title: 'Click to extend',
            chat_retracted_title: 'Click to retract'
        },
        stateMap = {
            $container: null,
            is_chat_retracted: true,
            anchor_map: {}
        },
        jqueryMap = {},
        setJqueryMap, toggleChat, onClickChat, initModule, copyAnchorMap, changeAnchorPart, onHashchange;
    //
    copyAnchorMap = function(){
        return $.extend(true, {}, stateMap.anchor_map)
    }
    // changeAnchorPart工具方法对锚进行原子更新
    changeAnchorPart = function(arg_map){
        var
            anchor_map_revise = copyAnchorMap(),
            bool_return = true,
            key_name,
            key_name_dep;
        KEYVAL:
        for(key_name in arg_map){
            if(arg_map.hasOwnProperty(key_name)){
                if(key_name.indexOf('_') === 0){
                    continue KEYVAL;
                }
                anchor_map_revise[key_name] = arg_map[key_name];
                key_name_dep = '_' + key_name;
                if(arg_map[key_name_dep]){
                    anchor_map_revise[key_name_dep] = arg_map[key_name_dep];
                }else{
                    delete anchor_map_revise[key_name_dep];
                    delete anchor_map_revise['_s' + key_name_dep];
                }
            }
        }

        try{
            $.uriAnchor.setAnchor(anchor_map_revise);
        } catch(error){
            $.uriAnchor.setAnchor(stateMap.anchor_map, null, true);
            bool_return = false;
        }
        return bool_return;
    }
    // hash改变处理函数
    onHashchange = function(event){
        var anchor_map_previous = copyAnchorMap(),
            anchor_map_proposed,
            _s_chat_previous,
            _s_chat_proposed,
            s_chat_proposed;

        try{
            anchor_map_proposed = $.uriAnchor.makeAnchorMap();
        }catch(error){
            $.uriAnchor.setAnchor(anchor_map_previous, null, true);
            return false;
        }
        stateMap.anchor_map = anchor_map_proposed;

        _s_chat_previous = anchor_map_previous._s_chat;
        _s_chat_proposed = anchor_map_proposed._s_chat;
        /**
         * anchor_map_previous:{"chat":"open","_s_chat":"open"}
         * anchor_map_proposed:{"chat":"closed","_s_chat":"closed"}
         * _s_chat_previous:open
         * _s_chat_proposed:closed
         */
        console.log('anchor_map_previous:'+JSON.stringify(anchor_map_previous));
        console.log('anchor_map_proposed:'+JSON.stringify(anchor_map_proposed));
        console.log('_s_chat_previous:'+_s_chat_previous);
        console.log('_s_chat_proposed:'+_s_chat_proposed);

        if(!anchor_map_previous || _s_chat_previous !== _s_chat_proposed){
            s_chat_proposed = anchor_map_proposed.chat;
            switch (s_chat_proposed){
                case 'open' : toggleChat(true); break;
                case 'closed' : toggleChat(false); break;
                default : toggleChat(false);
                    delete anchor_map_proposed.chat;
                    $.uriAnchor.setAnchor(anchor_map_proposed, null, true);
            }
        }
        return false;
    }
    // setJqueryMap 函数用来设置jQuery对象缓存
    setJqueryMap = function(){
        var $container = stateMap.$container;
        jqueryMap = {
            $container: $container,
            $chat: $container.find('.spa-shell-chat')
        };
    };
    // 切换聊天区状态
    toggleChat = function(do_extend, callback){
        var
            px_chat_ht = jqueryMap.$chat.height(),
            is_open = px_chat_ht === configMap.chat_extend_height,
            is_closed = px_chat_ht === configMap.chat_retract_height,
            is_sliding = !is_closed && !is_open;

        if(is_sliding){
            return false;
        }
        // 展开聊天区
        if(do_extend){
            jqueryMap.$chat.animate(
                {
                   height: configMap.chat_extend_height
                },
                configMap.chat_extend_time,
                function(){
                    jqueryMap.$chat.attr('title', configMap.chat_retracted_title);
                    stateMap.is_chat_retracted = false;
                    if(callback){
                        callback(jqueryMap.$chat);
                    }
                }
            );
            return true;
        }
        // 收缩聊天区
        jqueryMap.$chat.animate(
            {
                height: configMap.chat_retract_height
            },
            configMap.chat_retract_time,
            function(){
                jqueryMap.$chat.attr('title', configMap.chat_extended_title);
                stateMap.is_chat_retracted = true;
                if(callback){
                    callback(jqueryMap.$chat);
                }
            });
        return true;
    }
    // 聊天单击事件函数
    onClickChat = function(event){
        changeAnchorPart({
            'chat': (stateMap.is_chat_retracted ? 'open' : 'closed')
        });
        return false;
    }
    // 初始化模块
    initModule = function($container){
        stateMap.$container = $container;
        $container.html(configMap.main_html);
        setJqueryMap();

        $.uriAnchor.configModule({
            schema_map: configMap.anchor_schema_map
        });
        $(window).bind('hashchange', onHashchange).trigger('hashchange');
        // 聊天条的单击事件
        stateMap.is_chat_retracted = true;
        jqueryMap.$chat
            .attr('title', configMap.chat_extended_title)
            .click(onClickChat);


    };

    return {
        initModule: initModule
    };
})();